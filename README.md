# Lightmeter ControlCenter (prototype)

Welcome to Lightmeter, the Open Source email deliverability monitoring system.

## Supported Mail Transfer Agents

Currently Postfix MTA is supported. Future support for additional MTAs is planned.

## Installation

### Docker image

Get the latest docker image from Docker Hub.

The docker image needs the directory with the postfix logs to be readonly mapped to /logs
and will expose a http server on port tcp/3838.

The simplest way to run this image using the docker comamnd line is:

```bash
$ docker run -p 3838:3838 -v/var/mail/:/logs:ro lightmeter/lightmeter-controlcenter:latest
```

Then open your web browser on the url `http://your-domain.com:3838`.
